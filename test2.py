from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from sklearn import svm
import numpy as np

svc = svm.SVC(kernel='rbf', C = 10, gamma = 100)
#logreg = linear_model.LogisticRegression(C=1e5) # just a big number
digits = datasets.load_digits()
#print(digits.images[0])
#print(digits.target[0])

X_train, X_test, y_train, y_test = train_test_split(digits.images, digits.target, test_size=0.4, random_state=0)

X_train = X_train.reshape(len(X_train), -1)
X_test = X_test.reshape(len(X_test), -1)

svc.fit(X_train, y_train)
# predict
y_pred = svc.score(X_test, y_test)
print(y_pred)
